<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 8/9/18
 * Time: 6:24 PM
 */
namespace  MereHead\ModuleConnector\Modules;

class WalletsModuleService extends WalletsConnectionModule
{

    /**
     * Command for listening : ping
     * Pinging trade module server
     * @return mixed
     */
    public function ping() {
        $msg = [
            'command' => 'ping',
        ];
        return $this->makeCall($msg);
    }

}