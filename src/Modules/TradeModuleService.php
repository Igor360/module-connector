<?php

namespace  MereHead\ModuleConnector\Modules;
use MereHead\ModuleConnector\TradeServices\OrdersService;
use MereHead\ModuleConnector\TradeServices\UserService;
use MereHead\ModuleConnector\TradeServices\AssetsService;
use MereHead\ModuleConnector\TradeServices\ExchangeService;

/**
 * This class using for sending data to server
 * Class TradeModuleService
 * @package Modules
 */
class TradeModuleService extends TradeConnectionModule
{

    use AssetsService, OrdersService, UserService, ExchangeService;


    /**
     * Command for listening : ping
     * Pinging trade module server
     * @return mixed
     */
    public function ping() {
        $msg = [
            'command' => 'ping',
        ];
        return $this->makeCall($msg);
    }

}