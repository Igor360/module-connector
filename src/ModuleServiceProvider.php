<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 7/26/18
 * Time: 12:46 PM
 */

namespace MereHead\ModuleConnector;


use Illuminate\Support\ServiceProvider;
use MereHead\ModuleConnector\Modules\TradeModuleService;
use MereHead\ModuleConnector\Modules\WalletsModuleService;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $config = __DIR__.'/Config/config.php';

        $this->publishes([
            $config => config_path('moduleconnector.php'),
        ], 'config');

        $this->mergeConfigFrom($config, 'moduleconnector');
    }


    /**
     * @throws \Exception
     */
    public function boot() {
        $this->app->bind('trademodule', TradeModuleService::class);
        $this->app->bind('walletsmodule', WalletsModuleService::class);
    }

}