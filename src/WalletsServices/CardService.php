<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 8/10/18
 * Time: 3:52 PM
 */

namespace WalletsServices;


trait CardService
{
    public function getBankCard(int $account_id)
    {
        $msg = [
            'commands' => __TRAIT__ . '@' . __FUNCTION__,
            'data' => [
                'account_id' => $account_id
            ]
        ];
        return $this->makeCall($msg);
    }

    public function store(int $account_id, $card_number, $default)
    {
        $msg = [
            'commands' => __TRAIT__ . '@' . __FUNCTION__,
            'data' => [
                'account_id' => $account_id,
                'card_number' => $card_number,
                'default' => $default
            ]
        ];
        return $this->makeCall($msg);
    }

    public function update(int $account_id, int $id, $card_number, $default)
    {
        $msg = [
            'commands' => __TRAIT__ . '@' . __FUNCTION__,
            'data' => [
                'account_id' => $account_id,
                'id' => $id,
                'card_number' => $card_number,
                'default' => $default
            ]
        ];
        return $this->makeCall($msg);
    }


    public function delete(int $account_id, int $card_id){
        $msg = [
            'commands' => __TRAIT__ . '@' . __FUNCTION__,
            'data' => [
                'account_id' => $account_id,
                'card_id' => $card_id
            ]
        ];
        return $this->makeCall($msg);
    }
}