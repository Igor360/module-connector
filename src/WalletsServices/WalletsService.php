<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 8/9/18
 * Time: 6:28 PM
 */

namespace MereHead\ModuleConnector\WalletsServices;


trait WalletsService
{

    public function getWallets(int $account_id)
    {
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $account_id
            ]
        ];
        return $this->makeCall($msg);
    }

    public function getWallet(int $account_id, int $wallet_id){
        $msg  = [
            'commands' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $account_id,
                'wallet_id' => $wallet_id
            ]
        ];
        return $this->makeCall($msg);
    }


    public function createWithdrawalRequest(int $account_id, float $amount, int $wallet_id, string $address){
        $msg  = [
            'commands' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $account_id,
                'wallet_id' => $wallet_id,
                'amount' => $amount,
                'address' => $address
            ]
        ];
        return $this->makeCall($msg);
    }


    public function generateAddressForWallet(int $account_id, int $wallet_id){
        $msg  = [
            'commands' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $account_id,
                'wallet_id' => $wallet_id
            ]
        ];
        return $this->makeCall($msg);
    }


    public function getTransactions(int $account_id, int $wallet_id){
        $msg  = [
            'commands' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $account_id,
                'wallet_id' => $wallet_id
            ]
        ];
        return $this->makeCall($msg);
    }


    public function removeWebhooksForWallet(int $account_id, int $wallet_id){
        $msg  = [
            'commands' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $account_id,
                'wallet_id' => $wallet_id
            ]
        ];
        return $this->makeCall($msg);
    }


    public function createWebhooksForWallet(int $account_id, int $wallet_id){
        $msg  = [
            'commands' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $account_id,
                'wallet_id' => $wallet_id
            ]
        ];
        return $this->makeCall($msg);
    }
}