<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 8/8/18
 * Time: 3:13 PM
 */

namespace MereHead\ModuleConnector\TradeServices;


trait UserService
{

    /**
     * Command for listening : create_account
     * Create user account
     * @param int $user_id
     * @return mixed
     */
    public function createAccount(int $user_id){
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $user_id
            ]
        ];
        return $this->makeCall($msg);
    }


    /**
     *  Command for listening : create_user_fee
     * Create users fee
     * @param $user_id
     * @return array
     */
    public function createUserFee($user_id){
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $user_id
            ]
        ];
        return $this->makeCall($msg);
    }


    /**
     * Command for listening : get_balances
     * Get user balance
     * @param int $accountId
     * @return mixed
     */
    public function getBalances(int $accountId){
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $accountId,
            ],
        ];

        return $this->makeCall($msg);
    }

    /**
     * Command for listening : increase_balance
     * Increase user balance
     * @param int $assetId
     * @param int $accountId
     * @param float $amount
     * @return mixed
     */
    public function increaseBalance(int $assetId, int $accountId, float $amount){
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'asset_id' => $assetId,
                'account_id' => $accountId,
                'amount' => $amount,
            ],
        ];

        return $this->makeCall($msg);
    }


    /**
     * Command for listening : freeze_balance
     * Freeze user balance
     * @param int $assetId
     * @param int $accountId
     * @param float $amount
     * @return mixed
     */
    public function freezeBalance(int $assetId, int $accountId, float $amount){
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'asset_id' => $assetId,
                'account_id' => $accountId,
                'amount' => $amount,
            ],
        ];
        return $this->makeCall($msg);
    }

    /**
     * Command for listening : decrease_balance
     * Decrease user balance
     * @param int $assetId
     * @param int $accountId
     * @param float $amount
     * @return mixed
     */
    public function decreaseBalance(int $assetId, int $accountId, float $amount){
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'asset_id' => $assetId,
                'account_id' => $accountId,
                'amount' => $amount,
            ],
        ];
        return $this->makeCall($msg);
    }

}